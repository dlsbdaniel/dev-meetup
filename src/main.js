import Vue from 'vue'
import App from './App'
import router from './router'
import firebase from 'firebase'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import colors from 'vuetify/es5/util/colors'
import { store } from './store'
import DateFilter from './filters/date'
import AlertCmp from './components/shared/Alert'

Vue.use(Vuetify, {
  theme: {
    primary: colors.red.darken1,
    secondary: '#424242',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107'
  }
})

Vue.config.productionTip = false

Vue.filter('date', DateFilter)
Vue.component('app-alert', AlertCmp)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
  created () {
    firebase.initializeApp({
      apiKey: 'AIzaSyBJLIzkQIe4nOAr-xapPb6zCyHipaMSPq0',
      authDomain: 'devmeetup-f4991.firebaseapp.com',
      databaseURL: 'https://devmeetup-f4991.firebaseio.com',
      projectId: 'devmeetup-f4991',
      storageBucket: 'devmeetup-f4991.appspot.com',
      messagingSenderId: '871808243733'
    })
  }
})
